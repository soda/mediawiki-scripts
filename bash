#!/bin/bash
set -e
cd $MW_INSTALL_DIR
$MW_SCRIPTS_DIR/start
docker compose exec -u 0 mediawiki bash
